/*test AB-testing*/
function PageB () {

    /* --> Suppression sidebar <-- */
    let sidebar = document.querySelector('div.main > div.container > div.row > div.col-lg-3');
    sidebar.style.display="none";
    
    
    /* --> changement logo <-- */
    let logo = document.querySelector('.header-logo img');
    logo.src="img/logo-default.png"; /*pour récupérer une autre image*/
    /*logo.style.filter="blur(5px)";  pour rajouter un filtre sur une image*/
    
    
    /* --> changer image 1 carroussel <-- */
    let carousel = document.querySelector('div.owl-carousel div:nth-child(1) img');
    carousel.src="img/products/sac.jpg"; console.log(carousel)
    carousel.style.height="555px";
    /* --> changer image 2 carroussel <-- */
    let carousel2 = document.querySelector('div.owl-carousel div:nth-child(2) img');
    carousel2.src="img/products/sac2.jpg"; 
    carousel2.style.height="555px";
    /* --> changer image 3 carroussel <-- */
    let carousel3 = document.querySelector('div.owl-carousel div:nth-child(3) img');
    carousel3.src="img/products/sac3.jpg"; 
    carousel3.style.height="555px";
    /*document.querySelector('div.owl-carousel div:nth-child(3) img').src="img/products/sacamain3.jpg";  technique sans déclarer une variable*/

    
    /* --> agrandir carroussel <-- */
    let maincol = document.querySelector('div.main > div.container > div.row > div.col-lg-9');
    maincol.className="col-lg-12";
   
        
    /* --> déplacer les div <-- */
    let maindiv = document.querySelector('div.summary'); console.log(maindiv) /*div principale*/
    let div1 = document.querySelector('div.product-meta'); console.log(div1) /*div catégories*/
    let div2 = document.querySelector('form.cart'); console.log(div2) /*div pour ajouter au panier*/
    let div2bis = document.querySelector('form.cart > button.btn'); console.log(div2bis) /*bouton pour ajouter au panier*/

    maindiv.insertBefore(div1, div2); /*intervertir div1 et div2 */
    /*décaler div2*/
    div2.style.display="flex";
    div2.style.flexDirection="column";
    div2.style.alignItems="center";
    div2.style.marginTop="120px";
    /*modif taille bouton*/
    div2bis.style.fontSize="18px";

    
    /* --> définir une image différente à chaque fois en utilisant tableau <-- */
    let otherproducts = document.querySelectorAll('.masonry-loader > .row > .product > span > a:nth-child(2) > span > img'); console.log(otherproducts)
    otherproducts[0].src="img/products/camera.jpeg";
    otherproducts[1].src="img/products/golfbag.jpg";
    otherproducts[2].src="img/products/workout.jpg";
    otherproducts[3].src="img/products/luxurybag.jpg";

    
    /* --> définir une hauteur pour toutes les img <--*/
    otherproducts.forEach((hauteur) => {
        hauteur.style.height="255px";
    });
    console.log(otherproducts);
    
    
    /* --> modif button footer <-- */
    let button = document.querySelector('form#newsletterForm div.input-group span.input-group-append button.btn'); console.log(button)
    button.style.borderRadius="0";  
}
    
PageB();
